const smallSmilePath = "M36.05 197.54a43.47 35.91 0 0 0-.07 2.04 43.47 35.91 0 0 0 43.38 35.91 43.47 35.91 0 0 0 .09 0 43.47 35.91 0 0 0 43.47-35.91 43.47 35.91 0 0 0-.07-2.04h-12.48a35.91 35.91 0 0 1-30.54 30.54 35.91 35.91 0 0 1-.07 0 35.91 35.91 0 0 1-30.48-30.54z";
const bigSmilePath = "M79.36 235.5a43.47 35.91 0 0 1-43.38-35.91 43.47 35.91 0 0 1 .07-2.04h86.8a43.47 35.91 0 0 1 .07 2.04 43.47 35.91 0 0 1-43.47 35.91 43.47 35.91 0 0 1-.09 0z";

// ------------- status bars -------------
const statusBars = document.getElementsByClassName('progress-bar');
// ------------- cloud view -------------
const cloudHTML = document.getElementById('cloud');
const smallSmile = document.getElementById('smallSmile');
const bodyWidgets = document.getElementsByClassName('cloudBody');
const eyeWidgets = document.getElementsByClassName('eye');
const lvl1Widgets = document.getElementsByClassName('lvl1');
const lvl2Widgets = document.getElementsByClassName('lvl2');
const lvl3Widgets = document.getElementsByClassName('lvl3');
// ------------- restart button -------------
const restartButton = document.getElementById('restartButton');
// ------------- memory -------------
let restarted = false;
// ------------- controls -------------
const multiCollapseList = document.getElementsByClassName('multi-collapse');
// ------------- highscores -------------
const ageValue = document.getElementById('ageValue');
const highScoresList = document.getElementById('highScoresList');
const deathIcons = {
  'water': '<span class="sr-only">eau</span><i aria-hidden="true" class="fas fa-tint">',
  'heat': '<span class="sr-only">température</span><i aria-hidden="true" class="fas fa-thermometer-half">',
  'fun': '<span class="sr-only">fun</span><i aria-hidden="true" class="fas fa-laugh-squint">'
}
const highScoreCollapse = document.getElementById('highScoreCollapse');
const highScoreCollapseIcon = document.querySelector('#highScoreCollapse i');
const collapsed = 'fa-chevron-down';
const expanded = 'fa-chevron-up';


// ------------- BRIGHTER SMILE -------------
function smileBrighter(hover) {
  // when called by a ponctual event (touch, instead of hover)
  // will return the smile to normal after a delay
  // instead of waiting for another event
  if (!hover) {
    setTimeout(smileBackToNormal, 1000);
  }
  smallSmile.setAttribute('d', bigSmilePath);
  smallSmile.style.fill = 'red';
};

function smileBackToNormal() {
  smallSmile.setAttribute('d', smallSmilePath);
  smallSmile.style.fill = 'black';
};

cloudHTML.addEventListener('mouseover', () => {
  smileBrighter(true)
});
cloudHTML.addEventListener('mouseleave', smileBackToNormal);
cloudHTML.addEventListener('touchstart', () => {
  smileBrighter(false)
});


// ------------- STATUS BARS UPDATE FUNCTION -------------
function updateStatus() {
  for (let statusBar of statusBars) {
    // we read the value of this specific need in the cloud
    // knowing the statusBar's id = the need's name
    let value = cloud[statusBar.id];
    statusBar.style.width = `${value}%`;
    statusBar.setAttribute('aria-valuenow', value);
  }
}

// ------------- CLOUD ASPECT UPDATE FUNCTION -------------
// makes the eyes flash a certain color when clicking on buttons (or using the console)
function eyeFlash(need, full) {
  const delay = 1000;

  for (let eye of eyeWidgets) {
    eye.classList.add(`flash${need}`);

    if (full) {
      eye.classList.add('animateFull');
    }
    else {
      // for the cases when the animation is launched several times in quick succession
      eye.classList.remove('animateFull');
    }
    // reset the eye after animation end
    setTimeout(function () {
      eye.classList.remove(`flash${need}`);
      eye.classList.remove('animateFull');
    }, delay)
  }
}

// toggle visibility on certain parts of the cloud
// depending on the health
function updateCloud() {
  let health = cloud.getHealth();

  // if dead
  if (health <= 0) {
    for (let widget of lvl1Widgets) {
      widget.classList.add('fillTransparent');
    }
    for (let eye of eyeWidgets) {
      eye.classList.add('deathAnimation');
    }
    setTimeout(function() {
      restartButton.style.opacity = '1';
    }, 3000);
  }
  else {
    // if restarted
    if (restarted) {
      // remove death widgets
      for (let eye of eyeWidgets) {
        eye.classList.remove('deathAnimation');
      }
      restartButton.style.opacity = '0';
      // display once again a healthy cloud
      for (let widget of lvl1Widgets) {
        widget.classList.remove('fillTransparent');
      }
      for (let widget of lvl2Widgets) {
        widget.classList.add('fillTransparent');
      }
      for (let widget of lvl3Widgets) {
        widget.classList.add('fillTransparent');
      }
    }

    // cloud lvl1 -> hide lvl2 & lvl3
    if (health <= lvl1Health) {
      for (let widget of lvl1Widgets) {
        widget.classList.remove('fillTransparent');
      }
      for (let widget of lvl2Widgets) {
        widget.classList.add('fillTransparent');
      }
      for (let widget of lvl3Widgets) {
        widget.classList.add('fillTransparent');
      }

    } // cloud lvl2 -> hide lvl3
    else if (health <= lvl2Health) {
      for (let widget of lvl1Widgets) {
        widget.classList.remove('fillTransparent');
      }
      for (let widget of lvl2Widgets) {
        widget.classList.remove('fillTransparent');
      }
      for (let widget of lvl3Widgets) {
        widget.classList.add('fillTransparent');
      }
    } // cloud lvl3
    else {
      for (let widget of lvl1Widgets) {
        widget.classList.remove('fillTransparent');
      }
      for (let widget of lvl2Widgets) {
        widget.classList.remove('fillTransparent');
      }
      for (let widget of lvl3Widgets) {
        widget.classList.remove('fillTransparent');
      }
    }

  }

  // change the cloud's body opacity, depending on health
  for (let widget of bodyWidgets) {
    widget.style.opacity = `${(4/300)*health}`;
  }
}

// ------------- INTERFACE TOGGLE FUNCTIONS -------------
function toggleControls() {
  // TODO: trouver un moyen de le faire sans JQuery
  // $('.multi-collapse').collapse()
}

// changes the button aspect when clicked to show whether it'll collapse or expand
function toggleHighScoreList() {
  highScoreCollapseIcon.classList.toggle(collapsed);
  highScoreCollapseIcon.classList.toggle(expanded);
}
highScoreCollapse.addEventListener('click', toggleHighScoreList);

// ------------- RECORD KEEPING FUNCTIONS -------------
// update score display in highscore board
function updateScore() {
  ageValue.innerText = cloud.age;
}

// add score to the list in highscore board, customized according to the death
function displayScore (score, newScore = false) {
  // prepare score message
  let scoreBox = document.createElement('div');
  scoreBox.classList.add('score');
  scoreBox.innerHTML = '<span class="sr-only">Causes de la mort :</span>'; // for accessibility
  // add death causes
  let deathCausesIcons = document.createElement('div');
  deathCausesIcons.classList.add('deathCausesIcons');
  // add the corresponding icon for each death cause
  for (let cause of score.deathCauses) {
    deathCausesIcons.innerHTML += deathIcons[cause];
  }
  scoreBox.appendChild(deathCausesIcons);
  // add the score itself
  scoreBox.innerHTML += '<span class="sr-only"> Age :</span>';
  let ageBox = document.createElement('div');
  ageBox.innerText = score.age;
  scoreBox.appendChild(ageBox);

  // add it to the list, so the list stays sorted
  let index = 0;
  while (index < highScoresList.children.length) {
    let currentChildAge = parseInt(highScoresList.children[index].lastElementChild.innerText);
    if (currentChildAge <= score.age) {
      break;
    }
    index++;
  }
  // we break when outside the list, or the currently examined child's age is <= to the new one
  if (index === highScoresList.children.length) {
    highScoresList.appendChild(scoreBox);
  } else {
    highScoresList.insertBefore(scoreBox, highScoresList.children[index]);
  }

  // attract attention to the new score
  if (newScore) {
    let target;
    // if list is collapsed -> flash the icon
    if (highScoreCollapseIcon.classList.contains(collapsed)) {
      target = highScoreCollapse;
    }
    // else flash the score itself
    else {
      target = scoreBox;
    }

    // adds the animation
    target.classList.add('flashNew');
    // removes it after a delay, allowing for a repeat
    setTimeout(function() {
      target.classList.remove('flashNew');
    }, 2000);
  }
}



// ------------- MAIN UPDATE FUNCTION -------------
function updateView() {
  // update status bars
  updateStatus();
  // update cloud aspect
  updateCloud();
  // update current score
  updateScore();
}
