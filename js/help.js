// help open/close buttons
let helpOpen = document.getElementById('helpButton');
let helpClose = document.getElementById('helpClose');
// help pages
let helpP1 = document.getElementById('page1');
let helpP2 = document.getElementById('page2');
let helpP3 = document.getElementById('page3');
// help navigation arrows
let arrowPrevious = document.getElementById('previous');
let arrowNext =  document.getElementById('next');
// current page
let currentPage = 1;


// ------------ PAGE CHANGE FUNCTIONS ------------
function nextPage(e) {
  if(e.type == 'keydown' && e.key != 'Enter') {
    // we ignore keyboard keys other than 'Enter'
    return
  }

  switch (currentPage) {
    case 1:
      // changes current page
      helpP1.classList.toggle('d-none');
      helpP2.classList.toggle('d-none');
      // allow going back to the previous page
      arrowPrevious.classList.toggle('d-none');
      break;
    case 2:
      // changes current page
      helpP2.classList.toggle('d-none');
      helpP3.classList.toggle('d-none');
      // forbid going forward to the (nonexistent) next page
      arrowNext.classList.toggle('d-none');
      break;
    default:
      alert('DEBUG help arrows (next)');
  }

  currentPage++;
}

function previousPage(e) {
  if(e.type == 'keydown' && e.key != 'Enter') {
    // we ignore keyboard keys other than 'Enter'
    return
  }

  switch (currentPage) {
    case 2:
      // changes current page
      helpP2.classList.toggle('d-none');
      helpP1.classList.toggle('d-none');
      // forbid going to the (nonexistent) previous page
      arrowPrevious.classList.toggle('d-none');
      break;
    case 3:
      // changes current page
      helpP3.classList.toggle('d-none');
      helpP2.classList.toggle('d-none');
      // allow going back to the next page
      arrowNext.classList.toggle('d-none');
      break;
    default:
      alert('DEBUG help arrows (previous)');
  }

  currentPage--;
}

// ------------ GAME PAUSE / RESUME FUNCTIONS ------------
function pause() {
  clearInterval(timer);
}

function resume() {
  if (restarted) {
    timer = setInterval(update, updateDelay);
  }
}


// ------------ ATTACHING EVENT LISTENERS ------------
// page change
arrowPrevious.addEventListener('mouseup', previousPage);
arrowPrevious.addEventListener('touchstart', previousPage);
arrowPrevious.addEventListener('keydown', previousPage);
arrowNext.addEventListener('mouseup', nextPage);
arrowNext.addEventListener('touchstart', nextPage);
arrowNext.addEventListener('keydown', nextPage);
// pause / resume : so the game doesn't keep going in the background while
// we are reading the instructions
helpOpen.addEventListener('mouseup', pause);
helpOpen.addEventListener('touchstart', pause);
helpClose.addEventListener('mouseup', resume);
helpClose.addEventListener('touchstart', resume);
document.addEventListener('keydown', function(e) {
  if (e.key == 'Escape') {
    resume();
  }
});
