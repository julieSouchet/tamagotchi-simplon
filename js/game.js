// ------------- DEBUGGING -------------
let updateDelay = 500;


// ------------- CONTROLS FUNCTIONS -------------

// ------------- buttons control -------------
let waterButton = {'html': document.getElementById('waterButton'), 'eventListener': addWater};
let heatButton = {'html': document.getElementById('heatButton'), 'eventListener': addHeat};
let funButton = {'html': document.getElementById('funButton'), 'eventListener': addFun};
let buttonList = [waterButton, heatButton, funButton];

// ------------- keyboard controls -------------
let consoleInput = document.getElementById('consoleInput');
let consoleOutput = document.getElementById('consoleOutput');
let helpMessage = `Commandes disponibles :
\t "eau" : donne de l'eau au nuage,
\t "temp" : réchauffe le nuage,
\t "fun" : amuse le nuage,
\t "aide" ou "?" : affiche le message d'aide,
\t "recommencer" : recommence une partie,
\t "vider" : vide la liste des scores,
\t "exit" : quitte le mode console.`

function parseInput(event) {
  if(event.key === 'Enter') {
    // the user's input
    let command = consoleInput.value;
    // the game's answer
    let answer;

    // clears the input
    consoleInput.value = '';

    // copies the input in the output
    consoleOutput.value = consoleOutput.value.concat('\n> ' + command);

    let deathAnswer = "Il est mort :'( (monstre). Taper 'recommencer' pour lancer une nouvelle partie.";
    // check whether the command is valid
    switch (command) {
      // water command
      case 'eau':
      case 'Eau':
      if (restarted) {
        answer = 'Eau ajoutée !'
        addWater();
      } else {
        answer = deathAnswer;
      }
      break;
      // help command
      case 'temp':
      case 'Temp':
      if (restarted) {
        answer = 'Température augmentée !'
        addHeat();
      } else {
        answer = deathAnswer;
      }
      break;
      // fun command
      case 'fun':
      case 'Fun':
      if (restarted) {
        answer = 'Fun ! Fun ! Fun !'
        addFun();
      } else {
        answer = deathAnswer;
      }
      break;
      // help command
      case 'aide':
      case 'Aide':
      case '?':
      answer = helpMessage;
      break;
      // exit command
      case 'exit':
      case 'Exit':
      answer = 'Changement de contrôle... (TODO)';
      toggleControls();
      break;
      // restart command
      case 'recommencer':
      case 'Recommencer':
      if (restarted) {
        answer = 'Vous êtes déjà en pleine partie !'
      } else {
        answer = "Et c'est reparti !! :D";
        gameStart();
      }
      break;
      // restart command
      case 'vider':
      case 'Vider':
        answer = 'Liste de scores vidée.'
        clearHighScore();
      break;
      // other, unrecognized commands
      default:
      answer = 'Commande inconnue. Taper "?" ou "aide" pour voir les commandes disponibles.'
    }

    // adds the answer in the output
    consoleOutput.value = consoleOutput.value.concat('\n' + answer);

    // auto-scroll the output to the last line
    consoleOutput.scroll(0, consoleOutput.scrollHeight);
  }
}


// ------------- GAME ENGINE -------------

// ------------- game over functions -------------

function gameOver() {
  // stop the game running
  clearInterval(timer);
  // remove controls
  removeControls();
  // record score
  let score = recordScore();
  displayScore(score, true);
  // set all needs to zero
  cloud.clear();
  restarted = false;
}

// ------------- global update function -------------

function update() {
  // apply decay according to the rules
  cloud.tickDown();
  // check if game over ?
  if (cloud.getHealth() === 0) {
    gameOver();
  }
  // update the view (cloud, status, buttons...)
  updateView();
}

// ------------- starts the game -------------
var timer;


function gameStart(ev) {
  // set controls
  setControls();
  // set health
  cloud.reset();
  restarted = true;
  // starts the game running
  timer = setInterval(update, updateDelay);
}


// ------------- event listeners -------------
consoleInput.addEventListener('keydown', parseInput);

function setControls() {
  // control buttons now available
  for (let button of buttonList) {
    button.html.addEventListener('mouseup', button.eventListener);
    button.html.removeAttribute('disabled');
  }
  // restart button only available after game ended
  setTimeout(function() {
    restartButton.style.display = 'none';
  }, 1000);
}

function initialization() {
  pB.el.value = el.actuel;
  pB.el.max = el.max;
}


function removeControls() {
  // control buttons only available after game start
  for (let button of buttonList) {
    button.html.removeEventListener('mouseup', button.eventListener);
    button.html.setAttribute('disabled', 'true');
  }
  // restart button now available
  setTimeout(function() {
    restartButton.style.display = 'block';
  }, 2900);
}

// restart button is special (only available after game end)
restartButton.addEventListener('mouseup', gameStart);
restartButton.addEventListener('keydown', (ev) => {
  if (ev.key == 'Enter') {
    // we ignore keyboard keys other than 'Enter'
    gameStart()
  }
});

// highscore list clearing function
function clearHighScore() {
  localStorage.clear();
  highScoresList.innerHTML = '';
}

let hSDelete = document.getElementById('highScoreDelete');
hSDelete.addEventListener('keydown', function(e) {
  if (e.key == 'Enter') {
    clearHighScore();
  }
});
hSDelete.addEventListener('touchstart', clearHighScore);
hSDelete.addEventListener('click', clearHighScore);


// -------------------- actual starting point ! --------------------
initialize();
