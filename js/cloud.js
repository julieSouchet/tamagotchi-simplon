// ------------- CLOUD OBJECT -------------
// the various level of health our cloud can have
const maxHealth = 100,
  lvl2Health = 67,
  lvl1Health = 33;


let cloud = {
  // it dies if any of those needs fall to zero !
  water: maxHealth,
  heat: maxHealth,
  fun: maxHealth,
  // scorekeeping
  age: 0,
  id: 0,

  // get general status
  getHealth: function () {
    return Math.min(this.water, this.heat, this.fun);
  },

  // lose a certain amount of each need at each tick
  tickDown: function () {
    this.water--;
    this.heat--;
    this.fun--;
    this.age++;
  },

  reset: function () {
    this.water = maxHealth;
    this.heat = maxHealth;
    this.fun = maxHealth;
    this.age = 0;
  },

  clear: function () {
    this.water = 0;
    this.heat = 0;
    this.fun = 0;
  },

  getDeathCauses: function () {
    let deathCauses = [];
    if (this.water === 0) {
      deathCauses.push('water');
    }
    if (this.heat === 0) {
      deathCauses.push('heat');
    }
    if (this.fun === 0) {
      deathCauses.push('fun');
    }
    return deathCauses;
  }
};


function addWater() {
  let full = false;
  cloud.water += 20;
  if (cloud.water > maxHealth) {
    full = true;
    cloud.water = maxHealth;
  }
  eyeFlash('Water', full);
  updateView()
}

function addHeat() {
  let full = false;
  cloud.heat += 20;
  if (cloud.heat > maxHealth) {
    full = true;
    cloud.heat = maxHealth;
  }
  eyeFlash('Heat', full);
  updateView()
}

function addFun() {
  let full = false;
  cloud.fun += 20;
  if (cloud.fun > maxHealth) {
    full = true;
    cloud.fun = maxHealth;
  }
  eyeFlash('Fun', full);
  updateView()
}

function initialize() {
  // reads local storage for previous games
  let prevId = localStorage.getItem('gameNumber');
  if (prevId) {
    // get the latest id
    prevId = parseInt(prevId);
    // displays the previous scores
    for (let id = 0; id <= prevId; id++) {
      let score = JSON.parse(localStorage.getItem(id));
      displayScore(score);
    }
  }
  // starts the game
  gameStart();
}


function recordScore() {
  // updates cloud id
  let prevId = localStorage.getItem('gameNumber');
  // if none exist, set the id to 0
  if (!prevId) {
    cloud.id = 0
  } else {
    // else, iterate on the latest id
    cloud.id = parseInt(prevId)+1;
  }

  // creates a score object, containing the death causes, the age
  let score = {};
  // reads age
  score.age = cloud.age;
  // reads death causes
  score.deathCauses = cloud.getDeathCauses();

  // adds it to the localStorage
  localStorage.setItem('gameNumber', cloud.id);
  localStorage.setItem(cloud.id, JSON.stringify(score));

  // returns it for further uses
  return score;
}
